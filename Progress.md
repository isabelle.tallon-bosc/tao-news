# État du développement du RTC TAO

Les différentes tâches du RTC (au 29 juillet 2020) :

Glossaire :
- TAO = codé dans TAO (en C donc) ;
- C, C++, Julia, Yorick = codé dans ce langage (C++ = avec VCL) ;
- VCL = Vector Class Library ;

La priorité est critique si la tâche est nécessaire pour la prochaine mission.
L'hypothèse est que les r/w locks sont utilisés lors de la prochaine mission.

Il faudrait préciser l'architecture visée pour la prochaine mission : parties en C / Julia / yorick.

| Tâche / briques de calcul                   | État              | Commentaires / à faire                         | Priorité   |
|---------------------------------------------|-------------------|------------------------------------------------|------------|
| a. r/w lock                                 | validé (TAO)      | portage serveurs caméras                       | critique   |
| b. simulateur caméra                        | en cours (C)      | à finir, à tester                              |            |
| c. pré-traitement par serveur               | validé (C++)      | à intégrer aux serveurs, protocole             |            |
| d. coefficients équations mesures pentes    | validé (C)        | à intégrer au RTC (dll)                        |            |
| e. mesures des pentes avec/sans contraintes | validé (Julia)    | à intégrer au RTC (dll), retranscription en C  |            |
| f. calcul commande optimale                 | intégré (Julia)   |                                                |            |
| g. envoi commande optimale                  | intégré (Julia)   | serveur DM en C (en cours)                     | très basse |
| | | sauf que cela permettrait la calibration NCPA en boucle fermée. A moins d'une autre idée.
| h. intégration équations normales ref.      | validé (Julia)    | à intégrer au RTC                              |            |
|                                             |                   | retranscription en C pour serveur pentes       |            |
| i. résolution équations normales ref.       | validé (Julia)    | à intégrer au RTC                              |            |
|                                             |                   | retranscription en C pour serveur pentes et/ou |            |
|                                             |                   | éviter le ramasse miette de Julia              |            |
| j. extraction modes ref.                    | validé (Julia)    | à intégrer au RTC, gestion des bords           |            |
|                                             | en cours (C)      | version en C (dll)                             |            |
| k. valider l'algorithme de mesure           | validé  (Yorick)  | en boucle ouverte                              |            |
|                                             | à faire (Julia/C) | pour la version implantée dans le RTC          | critique   |
| l. multi-threading c/d+e/j                  | en cours (C)      | à part la gestion d'un thread-pool (TAO), tout |            |
|                                             |                   | reste à faire                                  |            |
| m. réglage non-supervisé pour les pentes    | en cours          | aucune méthode complètement satisfaisante identifiée pour le moment. | |
| | | -> le réglage automatique de mu marche bien. 
| n. auto-étalonnage                          | intégré           | pour le moment : réalisé préalablement         |            |
|                                             | à faire           | futur : en temps réel par perturbation         |            |

Le cycle temps réel pour valider la fermeture de la boucle sur le Soleil :
1. nouvelle image acquise par la caméra ;
2. pré-traitement, résultat : image pré-traitée et poids associés (tâche c) ;
3. pour chaque sous-image : calcul du déplacement par linéarisation du critère
   avec/sans contraintes (tâches d et e) ;
4. calcul de la commande optimale d'après les déplacements mesurés (tâche f) ;
5. envoi de la commande optimale (tâche g) ;
6. mise à jour de l'image de référence : intégration des coefficients des
   équations normales du problème quadratique avec contrainte temporelle (tâche
   h), résolution par gradients conjugués avec régularisation spatiale (tâche
   i) ;
7. pour chaque sous-image : extraction des composantes du modèle linéarisé
   (tâche j) ;

La première itération est différente car il faut estimer la première image de
référence mais, en gros, cela utilise les mêmes briques de calculs seul l'ordre
des étapes est changé.

L'extraction des modes (tâche j) est basée sur une méthode rapide
d'interpolation qui consiste essentiellement à calculer des corrélations
discrètes avec un petit noyau (la fonction d'interpolation ou sa dérivée) en
prenant en compte les conditions aux bords.  Dans le code Julia (validé),
l'image de référence est supposée toujours assez grande pour ne pas gérer les
conditions aux bords.  Dans le code en C (en cours), les conditions aux bords
sont gérées simplement par une copie intermédaire (cela simplifie et généralise
le code et permet de ne traiter que des corrélations d'éléments contigus et
donc d'exploiter très facilement la vectorisation).

Les tâches critiques en temps de calcul : toutes les tâches avant l'envoi des
commandes.  Gagner sur ces points permet de réduire le retard.  Par exemple, le
pré-traitement des images avec le bon découpage des étapes et la vectorisation
permettrait de réduire la durée de cette étape de 170µs à 90µs (autre gain
possible en parallélisant).  Le passage Julia -> C des étapes d et e permet un
gain en temps d'un facteur 2 ou 3.  Il y a sans doute un gain similaire à
attendre pour Julia -> C du calcul de la commande optimale (au moins pour le
ramasse miette, mais cette étape est rapide comparée aux autres).

Pour les autres étapes (h, i et j), la contrainte est que ça rentre dans le
temps restant avant l'image suivante.

En réalisant les étapes de la boucle par des processus différents (threads ou
serveurs) il est possible de gagner en flexibilité dans l'ordonnancement de la
boucle de contrôle, certaines étapes pouvant être réalisées de façon plus ou
moins asynchrone (mise à jour de l'image de référence et auto-étalonnage).
