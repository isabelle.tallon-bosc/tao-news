# TAO news

Here is reported in images and movies the progresses of the **adaptive optics**
(AO) system developped at CRAL for the **Themis solar telescope**.  The wavefront
sensing and the loop control of our system are implemented in an "inverse
problems" framework.  The real time conroller is built on TAO (a "Toolkit for
Adaptive Optics").

(TO DO: link references SPIE 2018,2019).

## December 3rd-12th 2020 on THEMIS

**12.04-06**
Optical adjustements of the WFS (WaveFront Sensor) when the Sun is observable
(cloudy conditions).

**12.07**
Open loop on the Sun granulation to check TAO's wavefront sensor.  The
reference image (the "direct model" for the wavefront sensor of TAO) is well
and reliably estimated.

(TO DO: Record a sequence on WFS detector with closed loop and insert image and
movie).

**12.08**
First closing of the loop on the solar granulation.

![first closing loop](media/images/TAO-201208-1017-sun-closing-loop-exp0.5.jpg)
Left: open loop -- right: closed loop.  Both images recorded with a temporal
smoothing of ~0.5s. Loop frequency is 1kHz ([see
movie](media/movies/TAO-201208-1017-sun-closing-loop-exp0.5.mov)).

The solar granulation is imaged through an H<sub>alpha</sub> filter on an Andor
detector and a 50" field-of-view (FOV). Here ~14.6" are visible. The center of
the images is conjugated with the center of the FOV of the WFS equal to ~10".

Other sequence, without any temporal smoothing: [see
movie](media/movies/TAO-201208-1323-sun-closing-loop.mov).

First closing of the loop on a sunspot.

![sunspot closing loop](media/images/TAO-201208-1414-sunspot-closing-loop.jpg)
Left: open loop -- right: closed loop. Loop frequency is 1kHz.  The FOV is ~25",
about 2.5 times larger than that of the wavefront sensor (WFS) whose center is
indicated by the green cross ([see
movie](media/movies/TAO-201208-1414-sunspot-closing-loop.mov)).

Another closing of the loop on the solar granulation with worse turbulence
conditions (lower elevation of the Sun and degraded seeing, r<sub>0</sub> estimated at
~3cm against ~5cm for the first closing loops of the morning).

![other clos loop](media/images/TAO-201208-1553-sun-closing-loop-exp0.5.jpg)
Left: open loop -- right: closed loop. Both images recorded with a temporal
smoothing of ~0.5s. Loop frequency is 1kHz ([see
movie](media/movies/TAO-201208-1553-sun-closing-loop-exp0.5.mov)).


**12.09**
Cloudy.
